#!/usr/bin/python3
import os
import datetime, requests
import yaml
from ruuvitag_sensor.ruuvi import RuuviTagSensor
from w1thermsensor import W1ThermSensor, Sensor

def html_header(file, title):
  file.write("<html>\n")
  file.write("<head>\n<style>\n")
  file.write("table { border: 2px solid;  border-collapse: collapse; }\n")
  file.write("td, th { border: 1px solid }\n")
  file.write("</style>\n<meta charset=\"utf-8\"></head>\n")
  file.write("<body><title>"+title+"</title>")
  file.write("<table><tr><th>Id</th><th>Namn</th><th>Temperatur</th><th>Tid</th></tr>")

def html_row(file, cssid, id, name, temp):
  file.write("<tr id=\""+cssid+"\">")
  file.write("<td>"+id+"</td>")
  file.write("<td>"+name+"</td>")
  file.write("<td id=\"temperature\">")
  file.write(str(temp))
  file.write("</td>")
  file.write("<td>"+str(datetime.datetime.now())+"</td>")
  file.write("</tr>")

def html_footer(file):
  file.write("</table>")
  file.write("</body></html>")

with open('temp2html.yaml') as f:
    conf = yaml.load(f, Loader=yaml.FullLoader)
f.close()

htmltitle = 'Temp2Html'
if 'htmltitle' in conf:
    htmltitle = conf['htmltitle']

htmlfile = 'index.html'
if 'htmlfile' in conf:
  htmlfilename = conf['htmlfile']

timeout_in_sec = 5

f = open(conf['htmldir']+".working.html", "w")

html_header(f, htmltitle)

if 'ruuvitags' in conf:
  for tag in conf['ruuvitags']:
    datas = RuuviTagSensor.get_data_for_sensors([tag['mac']], timeout_in_sec)
    html_row(f,tag['sensor'],tag['mac'],tag['name'],str(datas[tag['mac']]["temperature"]))

if 'w1sensors' in conf:
  for sensor in conf['w1sensors']:
    temperature = ""
    try:
      w1sensor = W1ThermSensor(Sensor.DS18B20, sensor['id'])
      temperature = str(round(w1sensor.get_temperature(),1))
    except:
      print("Unable to read sensor "+id)

    html_row(f,sensor['sensor'],sensor['id'],sensor['name'],temperature)

html_footer(f)

f.close()

os.rename(conf['htmldir']+".working.html",conf['htmldir']+htmlfile)
