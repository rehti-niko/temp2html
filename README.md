# temp2html

Simple script that reads RuuviTag and DS18B20 1-wire sensor temperature
values ad writes them to HTML. The HTML can be used by HomeAssistant 
Scrape integration.
